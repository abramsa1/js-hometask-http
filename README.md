# js-hometask-http

## Ответьте на вопросы

### 1. Из каких частей состоит HTTP-сообщение? Какие из этих частей обязательны?
> Ответ: HTTP сообщение состоит из 3 частей - стартовой строки, заголовков и тела сообщения. Обязательными являются стартовая строка и заголовки.  

### 2. Почему нехорошо делать запросы через XMLHttpRequest синхронно?
> Ответ: Потому что синхронный запрос "заморозит" страницу (заблокирует дальнейшее выполнение кода) до получения ответа от сервера, а так как скорость обработки запроса на сервере или скорость интернета может быть низкой - ожидание ответа может занять большое количество времени.

### 3. В статьях про XMLHttpRequest и Fetch API мы делали запрос к апи GitHub.
Был ли это кросс-доменный запрос?
Если да, то почему он выполнялся успешно?

    *Подсказка: используйте Chrome Developer Tools > Network > XHR для ответа на этот вопрос.*
> Ответ: Да, это кроссдоменный запрос. Он выполнился успешно, потому что в Access-Control-Allow-Origin заголовке ответа указано значение "*" которое разрешает запросы к "https://api.github.com/" из любых источников.

### 4. Исходный урл: `http://mywebpage.ru`. Какие из перечиcленных ниже урлов имеют один источник с исходным:
    
    * `http://www.mywebpage.ru/users`; - подходит
    * `https://mywebpage.ru`; - не подходит (другой протокол)
    * `http://mywebpage.ru/users`; - подходит
    * `http://mywebpage.ru:8080/users`; - не подходит (другой порт)
    * `http://mywebpage.org/users`. - не подходит (другой хост)
> Ответ: 1 и 3

### 5. Распределите запросы на простые и сложные:
    
    1 запрос:
    ```
    GET /search HTTP/1.1
    Host: site.com    
    ```
  
    2 запрос:
    ```
    POST /very/complicated/url HTTP/1.1
    Host: site.com
    Content-Type: multipart/form-data
    Accept-Language: *

    ..bodyContent..
    ```

    3 запрос:
    ```
    GET /user/data HTTP/1.1
    Host: site.com    
    Accept-Language: da, en-gb;q=0.8, en;q=0.7
    X-Authorization: 1AE2243CC232A2323DD
    ```

    Если эти запросы будут отправленны кросс-доменно, для каких из них выполнится preflight-запрос?
> Ответ: Простые запросы - 1 и 2. Запрос 3 - сложный, для него выполнится preflight запрос.


## Практическое задание

Сегодня мы будем учиться работать с сервером.

Сервер нужно установить, это делается командой: 
```
npm install -g json-server
```

Сервер запускается командой: 
```
json-server --watch db.json
```
Эту команду нужно выполнить в директории склонированного проекта, там где лежит файл README.

После этого откройте страничку в браузере по адресу: http://localhost:3000

Код нужно писать в файле ./public/app.js, для применения правок — перезагружайте страницу.

*Пользуйтесь Chrome Developer Tools > Network > XHR, чтобы смотреть какие запросы уходят на сервер, и какие ответы сервер возвращает. Там же можно посмотреть возникающие ошибки.*

*Не стесняйтесь пользоваться отладкой и ставить в коде точки останова: `debugger;`, а так же выводить промежуточные результаты в консоль. Главное, перед отправкой готового решения нам, удалите этот отладочный код :)*.

*Если сервер перестанет отвечать на запросы, просто перезапустите его в консоли*.

### Список задачек

1. Допишите функцию addUserFetch, используя метод fetch и учитывая следующие детали:

    - Функция должна принимать информацию о пользователе в виде js-объекта с полями, описывающими пользователя. 
    Cреди них не должно быть поля "id". Функция должна возвращать промис.

    - Добавлять пользователя на сервер нужно HTTP-методом POST, по урлу "/users", заголовок Content-Type должен иметь значение "application/json". 

    - В случае успешного добавления пользователя, сервер пришлет ответ с только что добавленными данными в формате JSON, и в этом объекте будет содержаться "id" пользователя. 
    Функция должна распарсить полученный JSON, и зарезолвить промис значением id добавленного пользователя;

    - В случае ошибки, например при передаче в функцию addUserXHR не объекта, функция должна реджектиться с ошибкой, в тексте которой должен содержаться код ошибки из ответа сервера и его текстовое описание.


2. Допишите функцию getUserFetch, используя метод fetch и учитывая следующие детали:

    - Функция должна принимать на вход число — id пользователя. Функция должна возвращать промис.

    - Получить пользователя можно по урлу "/users/{id}", HTTP-метод для получения — GET.

    - В случае успешного запроса пользователя, сервер пришлет ответ содержащий JSON c информацией о пользователе.
    Функция должна распарсить полученный JSON, и зарезолвить промис полученным js-объектом.

    - В случае ошибки, например при передаче в функцию getUserFetch id несуществующего пользователя, функция должна реджектиться с ошибкой, в тексте которой должен содержаться код ошибки из ответа сервера и его текстовое описание.

3. После реализации функций addUserFetch и getUserFetch должна заработать функция checkWork.
Запустите ее и проверьте что все работает без ошибок.

4. Реализуйте addUserXHR и getUserXHR используя XMLHttpRequest.

5. Перепишите функцию checkWork, чтобы она вызывала методы addUserXHR и getUserXHR.
