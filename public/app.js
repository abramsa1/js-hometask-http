'use strict';

function addUserFetch(userInfo) {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userInfo)
  }

  return fetch('/users', options)
    .then(response => response.status >= 400 ? Promise.reject(`${response.status} ${response.statusText}`) : response.json())
    .then(user => Promise.resolve(user.id))
}

function getUserFetch(id) {
  return fetch(`/users/${id}`)
    .then(response => response.status >= 400 ? Promise.reject(`${response.status} ${response.statusText}`) : response.json())
    .then(user => Promise.resolve(user))
}

function addUserXHR(userInfo) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '/users');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.addEventListener('load', () => {
      xhr.status >= 400 ? reject(`${xhr.status} ${xhr.statusText}`) : resolve(JSON.parse(xhr.responseText).id);
    });
    xhr.send(JSON.stringify(userInfo));
  })
}

function getUserXHR(id) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/users/${id}`);
    xhr.addEventListener('load', () => {
      xhr.status >= 400 ? reject(`${xhr.status} ${xhr.statusText}`) : resolve(JSON.parse(xhr.responseText));
    });
    xhr.send();
  })
}

function checkWork() {
  addUserXHR({
      name: "Alice",
      lastname: "XHR"
    })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

checkWork();